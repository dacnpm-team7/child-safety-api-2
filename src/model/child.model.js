const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Location = require('./location.model');

var ChildSchema = new mongoose.Schema(
    {
        key: String,
        name: String,
        locations: []
    },
    {
        timestamps: true
    }
);

var childs = mongoose.model('childs', ChildSchema);

module.exports = childs;